package com.twuc.webApp.web;

import com.twuc.webApp.domain.Producter;
import com.twuc.webApp.domain.ProducterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ShopController {
    @Autowired
    private ProducterRepository producterRepository;

    @GetMapping("/test")
    public String test(){
        return "hello world";
    }

    @GetMapping("/shop/product")
    public ResponseEntity<List<Producter>> getAllCommodity(){
        List<Producter> allCommodities = producterRepository.findAll();
        return ResponseEntity
                .status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(allCommodities);
    }

    @GetMapping("/shop/product/{id}")
    public ResponseEntity getTheOnlyCommodityInShop(@PathVariable Long id){
        Optional<Producter> commodityOptional = producterRepository.findById(id);
        if(!commodityOptional.isPresent()){
            return ResponseEntity.status(400).build();
        }
        return ResponseEntity
                .status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(commodityOptional.get());
    }

    @PostMapping("/shop/product")
    public ResponseEntity putCommodityInDb(@RequestBody @Valid Producter producter){
        String commodityName = producter.getName();
        Producter repositoryByName = producterRepository.findByName(commodityName);

        if(repositoryByName!=null){
            return ResponseEntity.status(400).body("the producter have in shop");
        }

        Producter producter1 = producterRepository.save(producter);
        producterRepository.flush();
        return ResponseEntity
                .status(200)
                .build();
    }

    @PostMapping("/shop/product/{id}")
    public ResponseEntity fixCommodityInDb(@PathVariable @Valid Long id, @RequestBody Producter producter){
        Optional<Producter> commodityOptional = producterRepository.findById(id);
        Producter producter1 = commodityOptional.get();
        if(producter1 ==null){
            return ResponseEntity.status(400).body("do not have that producter1");
        }

        producter1.setNumbers(producter.getNumbers()+ producter1.getNumbers());
        producter1.setName(producter.getName());
        producter1.setPrice(producter.getPrice());

        producterRepository.save(producter1);
        producterRepository.flush();

        return ResponseEntity
                .status(200)
                .build();
    }

}
