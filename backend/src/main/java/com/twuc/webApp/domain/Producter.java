package com.twuc.webApp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//这个是商品类
@Entity
@Table(name="product")
public class Producter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotNull
    private Long numbers;  //剩余的商品数量

    @Column(nullable = false)
    @NotNull
    @Size(min=1)
    private String name;  //商品的名字

    @Column(nullable = false)
    @NotNull
    private int price;

    @Column
    private String url;  //图片的url

    @Column
    private String unit;  //单位??? 字面意思



    public Producter(@NotNull Long numbers, @NotNull @Size(min = 1) String name, @NotNull int price, String url, String unit) {
        this.numbers = numbers;
        this.name = name;
        this.price = price;
        this.url = url;
        this.unit = unit;
    }

    public Producter() {
    }

    public Producter(Long numbers, String name, int price) {
        this.numbers = numbers;
        this.name = name;
        this.price = price;
    }

    public Long getNumbers() {
        return numbers;
    }

    public void setNumbers(Long numbers) {
        this.numbers = numbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
