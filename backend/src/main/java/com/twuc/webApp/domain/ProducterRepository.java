package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProducterRepository extends JpaRepository<Producter,Long> {
    Producter findByName(String name);
}
