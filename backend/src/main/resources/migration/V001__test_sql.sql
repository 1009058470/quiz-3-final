create table if not exists testForBackend(
    `id`            BIGINT              AUTO_INCREMENT PRIMARY KEY,
    `test_name`    VARCHAR(64)         NOT NULL,
    `last_name`     VARCHAR(64)         NOT NULL
)