create table product(
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY,
    `numbers` BIGINT NOT NULL,
    `name` VARCHAR(64) NOT NULL,
    `price` int NOT NULL
)