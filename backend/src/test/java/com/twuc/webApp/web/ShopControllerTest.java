package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.Producter;
import com.twuc.webApp.domain.ProducterRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ShopControllerTest extends ApiTestBase {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ProducterRepository producterRepository;

    @Test
    void db_is_work() {
        producterRepository.save(new Producter(1L, "cola", 1));
        producterRepository.flush();
        Optional<Producter> commodityOptional = producterRepository.findById(1L);
        Producter producter = commodityOptional.get();
        assertNotNull(producter);
    }

    @Test
    void should_return_all_commodities_in_shop() throws Exception {
        producterRepository.save(new Producter(1L, "aa", 1));
        producterRepository.save(new Producter(2L, "bb", 2));
        producterRepository.flush();
        mockMvc.perform(get("/api/shop/product"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[{\"numbers\":1,\"name\":\"aa\",\"price\":1},{\"numbers\":2,\"name\":\"bb\",\"price\":2}]"));
    }

    @Test
    void should_return_the_only_commodity_in_shop_have_it_id() throws Exception {
        producterRepository.save(new Producter(1L, "aa", 1));
        producterRepository.save(new Producter(2L, "bb", 1));
        producterRepository.flush();
        mockMvc.perform(get("/api/shop/product/1"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"numbers\":1,\"name\":\"aa\",\"price\":1}"));
    }

    @Test
    void should_save_in_db_when_post_a_real_commodity() throws Exception {
        mockMvc.perform(post("/api/shop/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numbers\":1,\"name\":\"aa\",\"price\":1}"))
                .andExpect(status().is(200));
        Optional<Producter> commodityOptional = producterRepository.findById(1L);
        Producter producter = commodityOptional.get();
        assertEquals(Long.valueOf(1), producter.getId());
    }

    @Test
    void should_add_numbers_when_a_commodity_have_been_in_db() throws Exception {
        producterRepository.save(new Producter(1L, "aa", 2));
        mockMvc.perform(post("/api/shop/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numbers\":1,\"name\":\"aa\",\"price\":1}"))
                .andExpect(status().is(400))
                .andExpect(content().string("the producter have in shop"));
        Optional<Producter> commodityOptional = producterRepository.findById(1L);
        Producter producter = commodityOptional.get();
        assertEquals(Long.valueOf(1), producter.getId());

    }

    @Test
    void should_fix_price_and_others_when_name_is_true() throws Exception {
        producterRepository.save(new Producter(1L, "aa", 2));
        mockMvc.perform(post("/api/shop/product/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numbers\":1,\"name\":\"bb\",\"price\":2}"))
                .andExpect(status().is(200));

        producterRepository.flush();
        assertNull(producterRepository.findByName("aa"));
        assertNotNull(producterRepository.findByName("bb"));// number可以增加  name 可以修改
    }

    @Test
    void should_add_url_and_unit_in_db_when_then_have() {
        producterRepository.save(new Producter(1L,"qq",1,"url","unit"));
        producterRepository.flush();
        Optional<Producter> producterOptional = producterRepository.findById(1L);
        Producter producter = producterOptional.get();
        assertEquals(producter.getUrl(),"url");
        assertEquals(producter.getUnit(),"unit");
    }
}