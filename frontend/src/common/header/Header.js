import React from "react";
import {Link} from "react-router-dom";

export const Header = () => {
  return (
    <header className={'Header'}>
      <Link to={"/"}>商城</Link>
      <Link to={"/order"}>订单</Link>
      <Link to={"/add"}>添加商品</Link>
    </header>
  );
};

export default Header;