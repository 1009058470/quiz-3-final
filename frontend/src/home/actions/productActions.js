export const getProduct = () => (dispatch) => {
  fetch('http://localhost:8080/api/shop/product')
    .then(response => response.json())
    .then(response => {
      console.log(response);
      dispatch({
        type: 'GET_PRODUCT',
        productList: response
      });
    });
};