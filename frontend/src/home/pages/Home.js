import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getProduct} from "../actions/productActions";
import {bindActionCreators} from "redux";
import Header from "../../common/header/Header";
import Footer from "../../common/footer/Footer";



class Home extends Component {

  componentDidMount() {
    this.props.getProduct();
  }


  constructor(props, context) {
    super(props, context);
    this.state = {
      value: ""
    };
  }

  render() {
    console.log(this.props.productList + "====================");
    const productList = this.props.productList;

    return (
      <div>
        <header>
          {/*<h1>我是真的是一个头</h1>*/}
          <Header></Header>
        </header>

        <section>
          {
            productList == null ? null :
              productList.map(value => {
                return <button key={value.id}>
                  <div>
                    <img src = "https://www.w3school.com.cn/i/eg_tulip.jpg"/>
                    <p>{value.name}</p>
                    <p>{"单价："+ value.price}</p>
                    <p>{"数量： "+value.numbers}</p>
                  </div>
                  </button> 
                
              })
          }
        </section>
        
        <Footer/>
        
      </div>
    );
  }
  
}

const mapStateToProps = state => {
  return {
    productList: state.product.productList
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
