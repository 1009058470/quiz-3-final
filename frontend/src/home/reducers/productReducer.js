const initState ={
  productList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_PRODUCT' :
      return {
        ...state,
        productList: action.productList
      };
    // case 'GET_DETAIL':
    //   return {
    //     ...state,
    //     noteDetail: action.noteDetail
    //   };
    default:
      return state;
  }
};