export const addProduct = (product) => (dispatch) => {
  console.log(product+"+++++++++++++++fetch");
  fetch('http://localhost:8080/api/shop/product' ,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(product)
  }).then(res => {
    dispatch({
      type: 'ADD_PRODUCT'
    });
  });
};