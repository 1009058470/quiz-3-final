import React from 'react';
import Footer from "../../common/footer/Footer";
import Header from "../../common/header/Header";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {addProduct} from "../actions/addproductAction";


export class AddProduct extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      disable: false//true
    }
  }
  
  addProductHandle(){
    console.log("button 提交");
    const product = {
      numbers: 20,
      // document.getElementById('numbers').value,
      name: document.getElementById('productName').value,
      price: document.getElementById('price').value
    };
    
    fetch('http://localhost:8080/api/shop/product' ,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(product)
    }).then(res => {
      console.log(res.status);
      console.log('打开button');
      this.setState({
        disable: false  //可以使用button
      });
      
    });
  }
  
  render() {
    return (
      <div>
        <Header/>

        <section>
          在这里开始
          <h3>添加商品</h3>

          <section>
            <label>名称</label>
            <input type="text" id={"productName"}/>
          </section>

          <section>
            <label>价格</label>
            <input type="text" id={"price"}/>
          </section>

          <section>
            <label>单位</label>
            <input type="text" id={"danWei"}/>
          </section>

          <section>
            <label>图片</label>
            <input type="text" id={"page"}/>
          </section>
          
          <button id = 'bt1' disabled={this.state.disable} className={'create'} onClick={this.addProductHandle.bind(this)}>
            提交
          </button>
          
        </section>
        
        <Footer/>
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    productList: state.product.productList
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);

