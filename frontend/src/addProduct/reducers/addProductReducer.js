const initState ={

};

export default (state = initState, action) => {
  switch (action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state
      };
    default:
      return state;
  }
};