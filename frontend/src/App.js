import React, {Component} from 'react';
import './App.less';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import Home from "./home/pages/Home";
import {AddProduct} from "./addProduct/pages/AddProduct";
import {Order} from "./order/pages/Order";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path={'/order'} component={Order} />
            <Route path={'/add'} component={AddProduct} />
            <Route path="/" component={Home} />
          </Switch>
        </Router>

      </div>
    );
  }
}

export default App;
