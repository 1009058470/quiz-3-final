import {combineReducers} from "redux";
import productReducer from "../home/reducers/productReducer";

const reducers = combineReducers({
  product: productReducer
});
export default reducers;